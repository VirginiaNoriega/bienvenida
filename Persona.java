public class Persona {
    private String nomre;
    private int documento;

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setDocumento(int numeroDocumento){
        this.documento = numeroDocumento;
    }

    public String getNombre(){
        return nombre; //o bien tambien this.nombre
    }

    public int getDocumento(){
        return documento;
    }

    
   @Overyde
    /*Se usa para evitar salidas por pantalla*/ public String toString(){
        return "Nombre: " + nombre + " Documento: " + documento;
    } //se encontraria definido tambien en la clase object

}