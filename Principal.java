public class Principal {
    public static void main(String[] args) {
        
        //se definen las variables
      //  int numero = 5;
       // double precio  = 52.00; //variables primitivas

      //j  tring nombre = "Pepe"; 

        //variable de tipo objeto o referencia de la clase saludo
        Saludo saludo1 = new Saludo(); //operador new para crear otro tipo de variable
         //constructor de la clase

         Saludo saludo2 =new Saludo();

         Saludo saludo3 =new Saludo(); 
         Saludo saludo4 =new Saludo();


            //se pude utilizar la variable de arriba para llamar al metodo

        saludo1.saludoDeMadrugada();

        saludo2.saludoDeTarde();

        saludo3.saludoDeNoche();

     //   System.out.println("Hola bienvenido al mundo java");

    //saludo1.saludoInicial = "Hola";
    saludo4.getSaludoInicial("Hola");
    System.out.println("Estado del Objeto: "+saludo4.getSaludoInicial()); //No se podria hacer referencia si esta en privado a menos que se defina un metodo para ello
    
    Persona persona1 = new Persona();

    persona1.setNombre("Pedro");
    persona1.setDocumento("7864522");

    System.out.println("Estado de persona1 " + persona1.getNombre() + persona1.getDocumento());
    
    System.out.println("Estado del objeto: " + persona4.toString());
}